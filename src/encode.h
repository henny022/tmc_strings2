#ifndef TMC_STRINGS2_ENCODE_H
#define TMC_STRINGS2_ENCODE_H

#include <string_view>
#include <cstdint>
#include <nlohmann/json.hpp>

void encode_string_to_buffer(std::string_view input, uint8_t *&target);

void encode_bank_to_buffer(const nlohmann::json &bank, uint8_t *&target);

void encode_table_to_buffer(const nlohmann::json &table, uint8_t *&target);

#endif //TMC_STRINGS2_ENCODE_H
