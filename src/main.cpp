#include "charmaps.h"
#include <cstdio>
#include <cstdlib>
#include <vector>
#include "fmt/format.h"
#include "CLI/CLI.hpp"
#include "nlohmann/json.hpp"
#include <iostream>
#include <filesystem>
#include "encode.h"

enum struct ParseType
{
    Table,
    Bank,
    String
};

namespace
{
std::string read_string(const std::string &input)
{
    if (input == "-")
    {
        fmt::print("reading input from stdin\n");
        return {std::istreambuf_iterator<char>(std::cin),
                std::istreambuf_iterator<char>()};
    }
    if (std::filesystem::exists(input))
    {
        fmt::print("reading input from file\n");
        std::ifstream f(input);
        return {std::istreambuf_iterator<char>(std::cin),
                std::istreambuf_iterator<char>()};
    }
    fmt::print("reading input from arg\n");
    return input;
}

nlohmann::json read_json(const std::string &input)
{
    if (input == "-")
    {
        fmt::print("reading input from stdin\n");
        return nlohmann::json::parse(std::cin);
    }
    if (std::filesystem::exists(input))
    {
        fmt::print("reading input from file\n");
        std::ifstream f(input);
        return nlohmann::json::parse(f);
    }
    fmt::print("reading input from arg\n");
    return nlohmann::json::parse(input);
}

bool encode_check_size(const uint8_t *start, uint8_t *&end, ptrdiff_t size)
{
    if (size)
    {
        if ((end - start) > size)
        {
            fmt::print(stderr, "error: result too big by {} bytes\n", (end - start) - size);
            return true;
        }
        while ((end - start) < size)
        {
            *end++ = 0xff;
        }
    }
    return false;
}

void write_buffer(const std::string &output, const uint8_t *start, const uint8_t *end)
{
    if (output == "-")
    {
        std::cout.write(reinterpret_cast<const char *>(start), end - start);
    }
    else
    {
        std::ofstream(output).write(reinterpret_cast<const char *>(start), end - start);
    }
}
}

int encode_string(const std::string &input, const std::string &output, ptrdiff_t size)
{
    auto s = read_string(input);

    // TODO implement some form of buffer overflow protection (check once per input or something)
    // TODO add a commandline option for buffer size
    auto buffer = std::make_unique<uint8_t[]>(0x100000);
    auto *end = buffer.get();

    try
    {
        encode_string_to_buffer(s, end);
    }
    catch (const std::runtime_error &e)
    {
        fmt::print(stderr, "parse failed, quitting\n");
        return 1;
    }

    if (encode_check_size(buffer.get(), end, size))
    { return 2; }

    write_buffer(output, buffer.get(), end);

    return 0;
}

int encode_bank(const std::string &input, const std::string &output, ptrdiff_t size)
{
    nlohmann::json bank = read_json(input);

    // TODO implement some form of buffer overflow protection (check once per input or something)
    // TODO add a commandline option for buffer size
    auto buffer = std::make_unique<uint8_t[]>(0x100000);
    auto *end = buffer.get();

    try
    {
        encode_bank_to_buffer(bank, end);
    }
    catch (const std::runtime_error &e)
    {
        fmt::print(stderr, "parse failed, quitting\n");
        return 1;
    }

    if (encode_check_size(buffer.get(), end, size))
    { return 2; }

    write_buffer(output, buffer.get(), end);

    return 0;
}

int encode_table(const std::string &input, const std::string &output, ptrdiff_t size)
{
    nlohmann::json table = read_json(input);

    // TODO implement some form of buffer overflow protection (check once per input or something)
    // TODO add a commandline option for buffer size
    auto buffer = std::make_unique<uint8_t[]>(0x100000);
    auto *end = buffer.get();

    try
    {
        encode_table_to_buffer(table, end);
    }
    catch (const std::runtime_error &e)
    {
        fmt::print(stderr, "parse failed, quitting\n");
        return 1;
    }

    if (encode_check_size(buffer.get(), end, size))
    { return 2; }

    write_buffer(output, buffer.get(), end);

    return 0;
}

int decode_string([[maybe_unused]] const std::string &input, [[maybe_unused]] const std::string &output)
{
    fmt::print(stderr, "decode string not implemented");
    return 1;
}

int decode_bank([[maybe_unused]] const std::string &input, [[maybe_unused]] const std::string &output)
{
    fmt::print(stderr, "decode bank not implemented");
    return 1;
}

int decode_table([[maybe_unused]] const std::string &input, [[maybe_unused]] const std::string &output)
{
    fmt::print(stderr, "decode bank not implemented");
    return 1;
}

int main(int argc, const char *argv[])
{
    CLI::App app{"tmc_strings2"};
    std::string input = "-";
    app.add_option(
            "-i,--input",
            input,
            "input, file or data (use \"-\" for stdin, default)"
    );
    std::string output = "-";
    app.add_option(
            "-o,--output",
            output,
            "output file (use \"-\" for stdout, default)"
    );
    bool decode = false;
    app.add_flag(
            "-e,--encode",
            [&decode](auto)
            {
                decode = false;
            },
            "encode mode (json to binary, default)"
    )->excludes(
            app.add_flag(
                    "-d,--decode",
                    [&decode](auto)
                    {
                        decode = true;
                    },
                    "decode mode (json to binary)"
            )
    );
    ParseType type = ParseType::Table;
    auto sflag = app.add_flag(
            "-s,--string",
            [&type](auto)
            {
                type = ParseType::String;
            },
            "process single string"
    );
    auto bflag = app.add_flag(
            "-b,--bank",
            [&type](auto)
            {
                type = ParseType::Bank;
            },
            "process one bank"
    );
    auto tflag = app.add_flag(
            "-t,--table",
            [&type](auto)
            {
                type = ParseType::Table;
            },
            "process whole table (default)"
    );
    ptrdiff_t size = 0;
    app.add_option(
            "--size",
            size,
            "output binary size. padding is applied if too small, error if too big. 0 for any size (default)"
    );

    sflag->excludes(bflag, tflag);
    bflag->excludes(sflag, tflag);
    tflag->excludes(sflag, bflag);

    try
    {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e)
    {
        return app.exit(e);
    }

    if (decode)
    {
        switch (type)
        {
            case ParseType::String:
                return decode_string(input, output);
            case ParseType::Table:
                return decode_table(input, output);
            case ParseType::Bank:
                return decode_bank(input, output);
        }
    }

    switch (type)
    {
        case ParseType::String:
            return encode_string(input, output, size);
        case ParseType::Table:
            return encode_table(input, output, size);
        case ParseType::Bank:
            return encode_bank(input, output, size);
    }
}
