#include "encode.h"
#include <tuple>
#include <vector>
#include <string>
#include <fmt/format.h>
#include "charmaps.h"

#if __cplusplus > 201703L
#define ASSERT_SYMBOL(map, symbol) static_assert(map[symbol] != nullptr);
#else
#define ASSERT_SYMBOL(map, symbol)\
if(map[symbol] == nullptr)\
{\
    fputs("no code found for symbol " #symbol "\n", stderr);\
    exit(1);\
}
#endif

enum struct ParseMode : char
{
    Normal,
    Hylian,
    Bold,
};

struct ParseState
{
    std::string_view input;
    size_t offset;
    ParseMode mode;
    const char *read_ptr;
    uint8_t *write_ptr;

    explicit ParseState(std::string_view input_, uint8_t *buffer) :
            input(input_),
            offset(0),
            mode(ParseMode::Normal),
            read_ptr(input.data()),
            write_ptr(buffer)
    {}
};

std::size_t replace_all(std::string &inout, std::string_view what, std::string_view with)
{
    std::size_t count{};
    for (
            std::string::size_type pos{};
            (pos = inout.find(what.data(), pos, what.length())) != std::string::npos;
            pos += with.length(), ++count
            )
    {
        inout.replace(pos, what.length(), with.data(), with.length());
    }
    return count;
}

void print_error(std::string_view error_message, const ParseState &state, int offset = 0) noexcept
{
    std::string input = state.input.data();
    replace_all(input, "\n", "\\n");
    replace_all(input, "\r", "\\r");
    fmt::print(stderr, "{}\n", error_message);
    fmt::print(stderr, "{}\n", input);
    fmt::print(stderr, "{0:->{1}}\n", "^", state.offset + offset + 1);
}

[[noreturn]] void throw_error(std::string_view error_message, const ParseState &state, int offset = 0) noexcept(false)
{
    print_error(error_message, state, offset);
    throw std::runtime_error(error_message.data());
}

template<size_t size>
bool try_write(ParseState &state, const CharMap<size> &map)
{
    auto entry = map.find(state.read_ptr);
    if (entry == nullptr)
    {
        return false;
    }
    if (map.prefix)
    {
        *state.write_ptr++ = map.prefix;
    }
    *state.write_ptr++ = static_cast<char>(entry->key);
    state.read_ptr += strlen(entry->value);
    state.offset += entry->len;
    return true;
}

struct Arg
{
    std::string_view text;
    int offset;

    [[nodiscard]] auto as_int() const
    {
        char *t;
        auto value = strtol(text.data(), &t, 16);
        auto len = t - text.data();
        if (t == text.data() || len != static_cast<long>(text.size()))
        {
            return std::make_tuple(len, false);
        }
        return std::make_tuple(value, true);
    }
};

template<size_t size>
struct FunctionMap
{
    using function_t = void (*)(const std::vector<Arg> &, ParseState &);

    struct Entry
    {
        const char *key;

        function_t value;
    };

    std::array<Entry, size> data;

    [[nodiscard]] constexpr function_t operator[](std::string_view key) const
    {
        const auto it = std::find_if(
                begin(data), end(data),
                [&key](const auto &entry)
                {
                    return std::string_view(entry.key) == key;
                });
        if (it != end(data))
        { return it->value; }
        return nullptr;
    }

    [[nodiscard]] constexpr function_t find(std::string_view string) const
    {
        const auto it = std::find_if(
                begin(data), end(data),
                [&string](const auto &entry)
                {
                    const auto result = string.rfind(entry.key, 0);
                    return result != std::string_view::npos;
                });
        if (it != end(data))
        { return it->value; }
        return nullptr;
    }
};

void write_ezlo_facing(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("EzloFacing takes 1 argument", state);
    }
    *state.write_ptr++ = char_map_ezlo_facing.prefix;
    auto value1 = char_map_ezlo_facing[args[0].text];
    if (value1 != nullptr)
    {
        *state.write_ptr++ = value1->key;
        return;
    }
    auto [value2, success] = args[0].as_int();
    if (success)
    {
        *state.write_ptr++ = value2;
        return;
    }
    throw_error("unknown facing direction", state, args[0].offset);
}

void write_button(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("Button takes 1 argument", state);
    }
    *state.write_ptr++ = char_map_button.prefix;
    auto value1 = char_map_button[args[0].text];
    if (value1 != nullptr)
    {
        *state.write_ptr++ = value1->key;
        return;
    }
    auto [value2, success] = args[0].as_int();
    if (success)
    {
        *state.write_ptr++ = value2;
        return;
    }
    throw_error("unknown button", state, args[0].offset);
}

void write_var(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("Var takes 1 argument", state);
    }
    auto [value, success] = args[0].as_int();
    if (!success)
    {
        throw_error("could not parse number from parameter", state, args[1].offset);
    }
    *state.write_ptr++ = 0x6;
    *state.write_ptr++ = value;
}

void write_color(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("Color takes 1 argument", state);
    }
    *state.write_ptr++ = char_map_color.prefix;
    auto value1 = char_map_color[args[0].text];
    if (value1 != nullptr)
    {
        *state.write_ptr++ = value1->key;
        return;
    }
    auto [value2, success] = args[0].as_int();
    if (success)
    {
        *state.write_ptr++ = value2;
        return;
    }
    throw_error("unknown color", state, args[0].offset);
}

void write_player(const std::vector<Arg> &args, ParseState &state)
{
    if (!args.empty())
    {
        throw_error("Player takes no arguments", state);
    }
    *state.write_ptr++ = 0x6;
    *state.write_ptr++ = 0x0;
}

void write_symbol(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("Symbol takes 1 argument", state);
    }
    auto [value, success] = args[0].as_int();
    if (!success)
    {
        throw_error("could not parse number from parameter", state, args[0].offset);
    }
    *state.write_ptr++ = char_map_symbol.prefix;
    *state.write_ptr++ = value;
}

void write_choice(const std::vector<Arg> &args, ParseState &state)
{
    if (args.empty())
    {
        throw_error("Choice requires at least one argument", state);
    }
    auto [value, success] = args[0].as_int();
    if (!success)
    {
        throw_error("could not parse number from parameter", state, args[0].offset);
    }
    *state.write_ptr++ = 0x5;
    *state.write_ptr++ = value;
    if (value != 0xff)
    {
        if (args.size() != 2)
        {
            throw_error("second argument required", state);
        }
        auto [value2, success2] = args[1].as_int();
        if (!success2)
        {
            throw_error("could not parse number from parameter", state, args[1].offset);
        }
        *state.write_ptr++ = value2;
        return;
    }
    if (args.size() != 1)
    {
        throw_error("too many arguments", state);
    }
}

void write_text(const std::vector<Arg> &args, ParseState &state)
{
    if (args.empty())
    {
        throw_error("choice requires at least one argument", state);
    }
    *state.write_ptr++ = 0x4;
    auto [value, success] = args[0].as_int();
    if (args[0].text == "BoxMove" || (success && value == 0x10))
    {
        *state.write_ptr++ = 0x10;
        if (args.size() < 2)
        {
            throw_error("second argument required", state);
        }
        if (args.size() > 2)
        {
            throw_error("too many arguments", state);
        }
        auto [value1, success1] = args[1].as_int();
        if (!success1)
        {
            throw_error("could not parse number from parameter", state, args[0].offset);
        }
        *state.write_ptr++ = value1;
        return;
    }
    if (args[0].text == "Reopen" || (success && value == 0x12))
    {
        *state.write_ptr++ = 0x12;
        if (args.size() > 1)
        {
            throw_error("too many arguments", state);
        }
        return;
    }
    if (args[0].text == "Center" || (success && value == 0x13))
    {
        *state.write_ptr++ = 0x13;
        if (args.size() > 1)
        {
            throw_error("too many arguments", state);
        }
        return;
    }
    if (args[0].text == "MonoStop" || (success && value == 0x14))
    {
        *state.write_ptr++ = 0x14;
        if (args.size() > 1)
        {
            throw_error("too many arguments", state);
        }
        return;
    }
    if (args[0].text == "MonoStart" || (success && value == 0x15))
    {
        *state.write_ptr++ = 0x15;
        if (args.size() > 1)
        {
            throw_error("too many arguments", state);
        }
        return;
    }
    if (success)
    {
        *state.write_ptr++ = value;
        if (args.size() < 2)
        {
            throw_error("second argument required", state);
        }
        if (args.size() > 2)
        {
            throw_error("too many arguments", state);
        }
        auto [value1, success1] = args[1].as_int();
        if (!success1)
        {
            throw_error("could not parse number from parameter", state, args[0].offset);
        }
        *state.write_ptr++ = value1;
        return;
    }
    throw_error("could not parse argument", state, args[0].offset);
}

void write_sound(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 1)
    {
        throw_error("Sound takes 1 argument1", state);
    }
    auto [value, success] = args[0].as_int();
    if (!success)
    {
        throw_error("could not parse number from parameter", state, args[0].offset);
    }
    *state.write_ptr++ = 0x3;
    *state.write_ptr++ = (value >> 8) & 0xff;
    *state.write_ptr++ = value & 0xff;
}

void write_text_jump(const std::vector<Arg> &args, ParseState &state)
{
    if (args.size() != 2)
    {
        throw_error("TextJump takes 2 arguments", state);
    }
    auto [value, success] = args[0].as_int();
    if (!success)
    {
        throw_error("could not parse number from parameter", state, args[0].offset);
    }
    auto [value1, success1] = args[1].as_int();
    if (!success1)
    {
        throw_error("could not parse number from parameter", state, args[1].offset);
    }
    *state.write_ptr++ = 0x7;
    *state.write_ptr++ = value;
    *state.write_ptr++ = value1;
}

void write_raw(const std::vector<Arg> &args, ParseState &state)
{
    for (const auto &arg: args)
    {
        auto [value, success] = arg.as_int();
        if (!success)
        {
            throw_error("could not parse number from parameter", state, arg.offset);
        }
        *state.write_ptr++ = value;
    }
}

FunctionMap<11> write_functions = {{{
                                            {"EzloFacing", write_ezlo_facing},
                                            {"Button", write_button},
                                            {"Var", write_var},
                                            {"Player", write_player},
                                            {"Color", write_color},
                                            {"Symbol", write_symbol},
                                            {"Choice", write_choice},
                                            {"Text", write_text},
                                            {"TextJump", write_text_jump},
                                            {"Sound", write_sound},
                                            {"Raw", write_raw},
                                    }}};

auto parse_command(ParseState &state)
{
    std::string_view in = state.read_ptr;
    auto len = in.find('}');
    if (len == std::string_view::npos)
    {
        throw_error("unterminated command", state);
    }
    std::vector<std::string_view> command;
    auto tptr = state.read_ptr;
    auto remaining = len;
    while (remaining > 0)
    {
        std::string_view rest(tptr, remaining);
        auto t = rest.find(':');
        if (t == std::string_view::npos)
        {
            command.push_back(rest);
            break;
        }
        command.emplace_back(tptr, t);
        tptr += t + 1;
        remaining -= t + 1;
    }
    auto name = command[0];
    std::vector<Arg> args;
    int offset = static_cast<int>(name.size()) + 1;
    for (size_t i = 1; i < command.size(); ++i)
    {
        args.push_back({command[i], offset});
        offset += static_cast<int>(command[i].size()) + 1;
    }
    state.read_ptr += len + 1;
    return std::make_tuple(name, args, len);
}

void encode_string_to_buffer(std::string_view input, uint8_t *&target)
{
    ParseState state(input, target);
    while (true)
    {
        if (*state.read_ptr == '\n')
        {
            *state.write_ptr++ = '\n';
            ++state.read_ptr;
            state.offset += 2;
            continue;
        }
        if (*state.read_ptr == 0)
        {
            *state.write_ptr++ = 0;
            target = state.write_ptr;
            return;
        }
        if (*state.read_ptr == '{')
        {
            ++state.read_ptr;
            ++state.offset;
            if (*state.read_ptr == '{')
            {
                *state.write_ptr++ = char_map_symbol.prefix;
                ASSERT_SYMBOL(char_map_symbol, "{");
                *state.write_ptr++ = static_cast<char>(char_map_symbol["{"]->key);
                ++state.offset;
                ++state.read_ptr;
                continue;
            }

            auto [command, args, command_len] = parse_command(state);
            auto tf = write_functions[command];
            if (tf == nullptr)
            {
                throw_error(fmt::format("unknown command \"{}\"", command), state);
            }
            tf(args, state);

            state.offset += command_len + 1;
            continue;
        }
        if (*state.read_ptr == '}')
        {
            ++state.read_ptr;
            ++state.offset;
            if (*state.read_ptr == '}')
            {
                ++state.read_ptr;
                ++state.offset;
            }
            else
            {
                print_error("unescaped '}'", state);
            }
            *state.write_ptr++ = char_map_symbol.prefix;
            ASSERT_SYMBOL(char_map_symbol, "}");
            *state.write_ptr++ = static_cast<char>(char_map_symbol["}"]->key);
            continue;
        }
        if (try_write(state, char_map))
        {
            continue;
        }
        if (try_write(state, char_map_kanji))
        {
            continue;
        }
        if (try_write(state, char_map_symbol))
        {
            continue;
        }
        throw_error("bad input", state);
    }
}

void encode_bank_to_buffer(const nlohmann::json &bank, uint8_t *&target)
{
    auto *entries_offsets = reinterpret_cast<uint32_t *>(target);
    auto n_entries = bank.size();
    auto *entries_start = target + n_entries * sizeof(uint32_t);
    auto *entry_start = entries_start;
    for (const auto &e: bank)
    {
        *entries_offsets++ = entry_start - target;
        encode_string_to_buffer(e.get<std::string>(), entry_start);
    }
    // align to 16 byte
    while ((entry_start - entries_start) % 16)
    {
        *entry_start++ = 0xff;
    }
    target = entry_start;
}

void encode_table_to_buffer(const nlohmann::json &table, uint8_t *&target)
{
    auto *banks_offsets = reinterpret_cast<uint32_t *>(target);

    auto n_banks = table.size();
    auto *bank_start = target + n_banks * sizeof(uint32_t);

    for (const auto &bank: table)
    {
        *banks_offsets++ = bank_start - target;
        encode_bank_to_buffer(bank, bank_start);
    }
    // align to 16 byte
    while ((bank_start - target) % 16)
    {
        *bank_start++ = 0xff;
    }
    target = bank_start;
}
