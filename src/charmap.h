#ifndef TMC_STRINGS2_CHARMAP_H
#define TMC_STRINGS2_CHARMAP_H

#include <algorithm>
#include <array>
#include <cstdint>
#include <cstring>
#include <string_view>

template<size_t size>
struct CharMap
{
    struct Entry
    {
        uint8_t key;
        uint8_t len;
        const char *value;
    };
    uint8_t prefix;
    std::array<Entry, size> data;

    [[nodiscard]] constexpr const Entry *operator[](uint8_t key) const
    {
        const auto it = std::find_if(
                begin(data), end(data),
                [&key](const auto &entry)
                {
                    return entry.key == key;
                });
        if (it != end(data))
        { return it; }
        return nullptr;
    }

    [[nodiscard]] constexpr const Entry *operator[](std::string_view value) const
    {
        const auto it = std::find_if(
                begin(data), end(data),
                [&value](const auto &entry)
                {
                    return std::string_view(entry.value) == value;
                });
        if (it != end(data))
        { return it; }
        return nullptr;
    }

    [[nodiscard]] constexpr const Entry *find(std::string_view string) const
    {
        const auto it = std::find_if(
                begin(data), end(data),
                [&string](const auto &entry)
                {
                    const auto result = string.rfind(entry.value, 0);
                    return result != std::string_view::npos;
                });
        if (it != end(data))
        { return it; }
        return nullptr;
    }
};

#endif //TMC_STRINGS2_CHARMAP_H
