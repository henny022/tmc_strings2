import xml.etree.ElementTree as ET

tree = ET.parse('TextConvert.xml')
root = tree.getroot()
with open('../src/charmaps.h', 'w', encoding='utf8', newline='\n') as h_file:
    h_file.write('#include "charmap.h"\n\n')
    for set in root:
        page = int(set.attrib['value'], 0)
        if page == 0:
            name = ''
        elif page == 0x1:
            name = 'ezlo_facing'
        elif page == 0x2:
            name = 'color'
        elif page == 0xb:
            if 'config' not in set.attrib:
                name = 'hylian_common'
            else:
                name = f'hylian_{set.attrib["config"].lower()}'
        elif page == 0xc:
            if 'mode' in set.attrib:
                assert set.attrib['mode'] == 'Bold'
                name = 'bold'
            else:
                name = set.attrib['command'].lower()
        elif page == 0xd:
            name = 'kanji'
        elif page == 0xe:
            continue
        elif page == 0xf:
            name = 'symbol'
        else:
            continue
        if name:
            name = f'char_map_{name}'
        else:
            name = 'char_map'
        lines = []
        for data in set:
            code = data.attrib['data']
            value = data.attrib['value']
            if value:
                size = len(value)
                if value == '"':
                    value = '\\"'
                lines.append(f'    {{{code}, {size}, "{value}"}},\n')
        lines = [f'\nconstexpr CharMap<{len(lines)}> {name}{{{page:#x},{{{{\n'] + lines + ['}}};\n']
        h_file.writelines(lines)
